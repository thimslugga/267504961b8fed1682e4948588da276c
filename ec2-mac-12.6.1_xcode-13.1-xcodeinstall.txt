% ssh ec2-user@ec2machost

    ┌───┬──┐   __|  __|_  )
    │ ╷╭╯╷ │   _|  (     /
    │  └╮  │  ___|\___|___|
    │ ╰─┼╯ │  Amazon EC2
    └───┴──┘  macOS Monterey 12.6.1

ec2-user@ip-172-31-46-187 ~ % brew tap sebsto/macos
Running `brew update --auto-update`...
==> Homebrew is run entirely by unpaid volunteers. Please consider donating:
  https://github.com/Homebrew/brew#donations
  
==> Tapping sebsto/macos
Cloning into '/opt/homebrew/Library/Taps/sebsto/homebrew-macos'...
remote: Enumerating objects: 158, done.
remote: Counting objects: 100% (2/2), done.
remote: Compressing objects: 100% (2/2), done.
remote: Total 158 (delta 0), reused 2 (delta 0), pack-reused 156
Receiving objects: 100% (158/158), 28.55 KiB | 4.08 MiB/s, done.
Resolving deltas: 100% (50/50), done.
Tapped 1 formula (13 files, 49.2KB).

ec2-user@ip-172-31-46-187 ~ % brew install xcodeinstall
==> Auto-updated Homebrew!
Updated 1 tap (homebrew/core).

You have 7 outdated formulae installed.
You can upgrade them with brew upgrade
or list them with brew outdated.

==> Fetching sebsto/macos/xcodeinstall
==> Downloading https://github.com/sebsto/xcodeinstall/releases/download/v0.6/xcodeinstall-0.6.arm64_monterey.bottle.tar.gz
==> Downloading from https://objects.githubusercontent.com/github-production-release-asset-2e65be/530541379/827b822c-229d-46e5-a8ed-89c2e7ddfd48?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAIWNJYAX4CSVEH53A%2F20230126%2Fus-east-1%2Fs3%2Faws4_re
######################################################################## 100.0%
==> Installing xcodeinstall from sebsto/macos
==> Pouring xcodeinstall-0.6.arm64_monterey.bottle.tar.gz
🍺  /opt/homebrew/Cellar/xcodeinstall/0.6: 7 files, 46.8MB
==> Running `brew cleanup xcodeinstall`...
Disable this behaviour by setting HOMEBREW_NO_INSTALL_CLEANUP.
Hide these hints with HOMEBREW_NO_ENV_HINTS (see `man brew`).

ec2-user@ip-172-31-46-187 ~ % xcodeinstall authenticate
Retrieving Apple Developer Portal credentials...
⚠️⚠️ We prompt you for your Apple ID username, password, and two factors authentication code.
These values are not stored anywhere. They are used to get an Apple session ID. ⚠️⚠️

Alternatively, you may store your credentials on AWS Secrets Manager
⌨️  Enter your Apple ID username: 
⌨️  Enter your Apple ID password:
Authenticating...
🔐 Two factors authentication is enabled, enter your 2FA code:
✅ Authenticated with MFA.

ec2-user@ip-172-31-46-187 ~ % xcodeinstall download --name "Xcode 13.1.xip"
                        Downloading Xcode 13.1
100% [============================================================] 10093 MB / 34.33 MBs
[ OK ]
✅ Xcode 13.1.xip downloaded

ec2-user@ip-172-31-46-187 ~ % mv ~/.xcodeinstall/download/Xcode\ 13.1.xip /Applications/Xcode-13.1.xip

ec2-user@ip-172-31-46-187 ~ % cd /Applications

ec2-user@ip-172-31-46-187 /Applications % sudo xip -x /Applications/Xcode-13.1.xip
xip: signing certificate was "Software Update" (validation not attempted)
xip: expanded items from "/Applications/Xcode-13.1.xip"

ec2-user@ip-172-31-46-187 /Applications % sudo xcode-select -s /Applications/Xcode.app/Contents/Developer

ec2-user@ip-172-31-46-187 /Applications % sudo xcodebuild -license accept
objc[4101]: Class AppleTypeCRetimerRestoreInfoHelper is implemented in both /usr/lib/libauthinstall.dylib (0x20b8ab650) and /Library/Apple/System/Library/PrivateFrameworks/MobileDevice.framework/Versions/A/MobileDevice (0x108de0598). One of the two will be used. Which one is undefined.
objc[4101]: Class AppleTypeCRetimerFirmwareAggregateRequestCreator is implemented in both /usr/lib/libauthinstall.dylib (0x20b8ab6a0) and /Library/Apple/System/Library/PrivateFrameworks/MobileDevice.framework/Versions/A/MobileDevice (0x108de05e8). One of the two will be used. Which one is undefined.
objc[4101]: Class AppleTypeCRetimerFirmwareRequestCreator is implemented in both /usr/lib/libauthinstall.dylib (0x20b8ab6f0) and /Library/Apple/System/Library/PrivateFrameworks/MobileDevice.framework/Versions/A/MobileDevice (0x108de0638). One of the two will be used. Which one is undefined.
objc[4101]: Class ATCRTRestoreInfoFTABFile is implemented in both /usr/lib/libauthinstall.dylib (0x20b8ab740) and /Library/Apple/System/Library/PrivateFrameworks/MobileDevice.framework/Versions/A/MobileDevice (0x108de0688). One of the two will be used. Which one is undefined.
objc[4101]: Class AppleTypeCRetimerFirmwareCopier is implemented in both /usr/lib/libauthinstall.dylib (0x20b8ab790) and /Library/Apple/System/Library/PrivateFrameworks/MobileDevice.framework/Versions/A/MobileDevice (0x108de06d8). One of the two will be used. Which one is undefined.
objc[4101]: Class ATCRTRestoreInfoFTABSubfile is implemented in both /usr/lib/libauthinstall.dylib (0x20b8ab7e0) and /Library/Apple/System/Library/PrivateFrameworks/MobileDevice.framework/Versions/A/MobileDevice (0x108de0728). One of the two will be used. Which one is undefined.
ec2-user@ip-172-31-46-187 /Applications % sudo xcodebuild -runFirstLaunch
objc[4104]: Class AppleTypeCRetimerRestoreInfoHelper is implemented in both /usr/lib/libauthinstall.dylib (0x20b8ab650) and /Library/Apple/System/Library/PrivateFrameworks/MobileDevice.framework/Versions/A/MobileDevice (0x107720598). One of the two will be used. Which one is undefined.
objc[4104]: Class AppleTypeCRetimerFirmwareAggregateRequestCreator is implemented in both /usr/lib/libauthinstall.dylib (0x20b8ab6a0) and /Library/Apple/System/Library/PrivateFrameworks/MobileDevice.framework/Versions/A/MobileDevice (0x1077205e8). One of the two will be used. Which one is undefined.
objc[4104]: Class AppleTypeCRetimerFirmwareRequestCreator is implemented in both /usr/lib/libauthinstall.dylib (0x20b8ab6f0) and /Library/Apple/System/Library/PrivateFrameworks/MobileDevice.framework/Versions/A/MobileDevice (0x107720638). One of the two will be used. Which one is undefined.
objc[4104]: Class ATCRTRestoreInfoFTABFile is implemented in both /usr/lib/libauthinstall.dylib (0x20b8ab740) and /Library/Apple/System/Library/PrivateFrameworks/MobileDevice.framework/Versions/A/MobileDevice (0x107720688). One of the two will be used. Which one is undefined.
objc[4104]: Class AppleTypeCRetimerFirmwareCopier is implemented in both /usr/lib/libauthinstall.dylib (0x20b8ab790) and /Library/Apple/System/Library/PrivateFrameworks/MobileDevice.framework/Versions/A/MobileDevice (0x1077206d8). One of the two will be used. Which one is undefined.
objc[4104]: Class ATCRTRestoreInfoFTABSubfile is implemented in both /usr/lib/libauthinstall.dylib (0x20b8ab7e0) and /Library/Apple/System/Library/PrivateFrameworks/MobileDevice.framework/Versions/A/MobileDevice (0x107720728). One of the two will be used. Which one is undefined.
Install Started
1%.........20.........40.........60.........80.Install Succeeded

ec2-user@ip-172-31-46-187 /Applications % sudo xcodebuild -version
objc[4725]: Class AppleTypeCRetimerRestoreInfoHelper is implemented in both /usr/lib/libauthinstall.dylib (0x20b8ab650) and /Library/Apple/System/Library/PrivateFrameworks/MobileDevice.framework/Versions/A/MobileDevice (0x1049b0598). One of the two will be used. Which one is undefined.
objc[4725]: Class AppleTypeCRetimerFirmwareAggregateRequestCreator is implemented in both /usr/lib/libauthinstall.dylib (0x20b8ab6a0) and /Library/Apple/System/Library/PrivateFrameworks/MobileDevice.framework/Versions/A/MobileDevice (0x1049b05e8). One of the two will be used. Which one is undefined.
objc[4725]: Class AppleTypeCRetimerFirmwareRequestCreator is implemented in both /usr/lib/libauthinstall.dylib (0x20b8ab6f0) and /Library/Apple/System/Library/PrivateFrameworks/MobileDevice.framework/Versions/A/MobileDevice (0x1049b0638). One of the two will be used. Which one is undefined.
objc[4725]: Class ATCRTRestoreInfoFTABFile is implemented in both /usr/lib/libauthinstall.dylib (0x20b8ab740) and /Library/Apple/System/Library/PrivateFrameworks/MobileDevice.framework/Versions/A/MobileDevice (0x1049b0688). One of the two will be used. Which one is undefined.
objc[4725]: Class AppleTypeCRetimerFirmwareCopier is implemented in both /usr/lib/libauthinstall.dylib (0x20b8ab790) and /Library/Apple/System/Library/PrivateFrameworks/MobileDevice.framework/Versions/A/MobileDevice (0x1049b06d8). One of the two will be used. Which one is undefined.
objc[4725]: Class ATCRTRestoreInfoFTABSubfile is implemented in both /usr/lib/libauthinstall.dylib (0x20b8ab7e0) and /Library/Apple/System/Library/PrivateFrameworks/MobileDevice.framework/Versions/A/MobileDevice (0x1049b0728). One of the two will be used. Which one is undefined.
Xcode 13.1
Build version 13A1030d